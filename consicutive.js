function get_absolute_difference(val1, val2, list_obj=None){
	if (list_obj != null){
		var diff = list_obj[val1] - list_obj[val2]
	}else{
		var diff = val1 - val2
	}
	
	if (diff == 1){
		return 1
	}
	
	else if (diff == -1){
			return -1
	}
	return 0;
}
	

function consecutive_finder(list_obj, consecutive_length=3){
        console.log("operating with list: " + list_obj);
		
        index_list = [];
        for (i=0; i <= list_obj.length - consecutive_length + 1; i++){
			first_diff = get_absolute_difference(i, i+1, list_obj)
            if (first_diff == 1 || first_diff == -1){
				if (get_absolute_difference(i+1, i+2, list_obj) == first_diff ){
				    index_list.push(i)
				}
			}
                
		}
		console.log(index_list);
        if (index_list != null){
			 return index_list
		}
        return ['No consicutive runs found!']
}


var test_list = [1, 2, 3, 5, 10, 9, 8, 9, 10, 11, 7]
consicutive_runs = consecutive_finder(test_list, 3)
console.log("\nResult: \n" + consicutive_runs)
